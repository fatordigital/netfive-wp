<?php
define('WP_USE_THEMES', false);
require_once('../../../wp-load.php');
?>
<!DOCTYPE html>

    <head>

		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link rel="stylesheet" type="text/css" href="style.css" />
		<script src="js/modernizr.custom.17475.js"></script>
    </head>
    <body>


            <div class="slider-geral">

				<ul id="carousel" class="elastislide-list">
					<?php
					$page_id = 1380;

					$attachments = get_children( array(
		        'post_parent' => $page_id,
		        'post_status' => 'inherit',
		        'post_type' => 'attachment',
		        'post_mime_type' => 'image',
		        'order' => 'ASC',
		        'orderby' => 'menu_order',
		        'numberposts' => 100
		    	) );

		    	if($attachments) {
					  foreach ( $attachments as $id  => $attachment ) {
			        $title = esc_html( $attachment->post_title, 1 );
			        $img = wp_get_attachment_image_src( $id, 'full' );

			       	?>
							<li>
								<img src="<?php echo $img[0]; ?>" alt="" />
							</li>
			       	<?php
			      }
	    		}

					?>
					<!-- <li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_vmware.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_software-one.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_microsoft.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_itil.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/faurgs.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/ecco_salva.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/credito_real.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/colegio_farroupilha.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/comil.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/condor.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/central_de_Imoveis.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/band.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_kaspersky.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_Linux_inst.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_ibm.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_hp.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_citrix.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/parc_fortinet.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/volpato.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/unicred.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/ulbra.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/techdec.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/soul.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/sperinde.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/stara.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/sij.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/rute_arcari.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/roglio.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/robustec.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/rede_pampa.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/papaleo_e_vieira.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/olfar.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/movix.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/mario_espindola.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/imobiliaria_city.jpg" alt="" /></li>
					<li><img src="http://netfive.aldeia.biz/wp-content/uploads/2014/02/fuga_couros.jpg" alt="" /></li> -->
				</ul>



			</div>
		</div>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquerypp.custom.js"></script>
		<script type="text/javascript" src="js/jquery.elastislide.js"></script>
		<script type="text/javascript">

			$( '#carousel' ).elastislide();

		</script>
    </body>
</html>