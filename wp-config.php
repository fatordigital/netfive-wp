<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'netfive');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '*&lIPzX:#m5Gmd-;B2n?K!-|LSVSt+YxGwT9pd`s%O!-uFB*Z=01LCD3:[)p|:2x');
define('SECURE_AUTH_KEY',  '|p~h:C6k|7OslC l]LqQtdF&--1<zPVYU9qYIku<@>pQ[6He|KBXta:ZZ3+r/}^S');
define('LOGGED_IN_KEY',    'xXAD|H@304795R71j8Wa`}GzFzcOsz4->a-z#{D49!BN2[AZy|CNI|*g:tXkyH6g');
define('NONCE_KEY',        '<<)Ys)X)L^G9h_pV(/0&u_6n_YcMk9r`|mXb]3|8a0v+Ot]~F$I8O04U X](~gYx');
define('AUTH_SALT',        '=NL9ldw6qhD~ApFy%PDm)>P2%!ia}0~k4+<y~urh{KkX1.egsJ] Sp:JNl-;)8{n');
define('SECURE_AUTH_SALT', 'x.(|Qn3a#QYm)<8 J&:*L{=lH*;p<xNv5H-4A#$e|gVvh^CsndqyFfC%N719(ash');
define('LOGGED_IN_SALT',   '%sNN1gv,MSKdJEgf3=%#A-DvVh~c9-5o/V`|+wUu}&>f+gPWDa,12%5OLFoiXnrD');
define('NONCE_SALT',       'Ak`::N|7.1*@rBIYj.-QSG}Qv{Gr7_zj+WfqouPZO+.#{_|Azh`{bIhfzd7,n)~r');
define( 'WP_AUTO_UPDATE_CORE', false );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * O idioma localizado do WordPress é o inglês por padrão.
 *
 * Altere esta definição para localizar o WordPress. Um arquivo MO correspondente ao
 * idioma escolhido deve ser instalado em wp-content/languages. Por exemplo, instale
 * pt_BR.mo em wp-content/languages e altere WPLANG para 'pt_BR' para habilitar o suporte
 * ao português do Brasil.
 */
define('WPLANG', 'pt_BR');

/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
