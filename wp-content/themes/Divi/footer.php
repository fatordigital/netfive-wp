<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix">
				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}
				?>

						<p id="footer-info">por Aldeia</p>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

    <!-- BEGIN: FORM MODAL -->
    <section class="fd_modal">
        <a href="javascript:void(0)" class="fd_close_modal">
            <img src="/wp-content/uploads/2014/02/close_modal-icon.png" alt="FECHAR [X] "/>
        </a>
        <div class="fd_container">
            <div class="fd_content">
                <form action="javascript:void(0)" class="fd_modal_form form-validate">
                    <h2>Quer entender como <strong>serviços gerenciados</strong> poderão impactar em sua empresa? </h2>
                    <div class="fd_message_return"></div>
                    <?php /* if (function_exists("add_formcraft_form")) { add_formcraft_form("[fc id='3'][/fc]"); } */ ?>
                    <div class="fd_row">
                        <div class="fd_col">
                            <input
                                    type="text"
                                    name="fd_name"
                                    class="fd_input"
                                    placeholder="Nome"
                                    required
                                    data-required="true"
                                    data-msg-required="Campo Obrigatório"
                            >
                        </div>
                        <div class="fd_col">
                            <input
                                    type="email"
                                    name="fd_email"
                                    class="fd_input"
                                    placeholder="E-mail"
                                    required
                                    data-required="true"
                                    data-msg-required="Campo Obrigatório"
                                    data-email="true"
                                    data-msg-email="Insira um email válido"
                            >
                        </div>
                    </div>
                    <div class="fd_row">
                        <div class="fd_col">
                            <input
                                    type="text"
                                    name="fd_phone"
                                    class="fd_input phone_mask"
                                    placeholder="Telefone"
                                    required
                                    data-required="true"
                                    data-msg-required="Campo Obrigatório"
                            >
                        </div>
                        <div class="fd_col">
                            <input
                                    type="text"
                                    name="fd_company"
                                    class="fd_input"
                                    placeholder="Empresa"
                                    required
                                    data-required="true"
                                    data-msg-required="Campo Obrigatório"
                            >
                        </div>
                    </div>
                    <div class="fd_row">
                        <div class="fd_col fd_col_100">
                            <textarea
                                    placeholder="Mensagem"
                                    name="fd_message"
                                    cols="30"
                                    rows="10"
                                    class="fd_input"
                                    required
                                    data-required="true"
                                    data-msg-required="Campo Obrigatório"
                            ></textarea>
                        </div>
                    </div>
                    <div class="fd_row fd_last_row">
                        <div class="fd_col">
                            <p>Você também pode ligar para nós e conversar com um consultor</p>
                        </div>
                        <div class="fd_col">
                            <h3><small>51</small> 3061-4446</h3>
                        </div>
                        <div class="fd_col">
                            <input type="hidden" name="fd_service">
                            <input type="hidden" class="fd_key">
                            <button type="submit" class="button submit-button">ENVIAR MENSAGEM</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!-- END: FORM MODAL -->

    <!-- BEGIN FD ABANDON WINDOW -->
    <div class="fd_backdrop"></div>
    <div class="fd_abandon_window">
        <a href="javascript:void(0)" class="fd_aw_close">X</a>
        <div class="fd_col">
            <h4>
                <small>NOVO E-BOOK</small>
                IMPACTO<br />
                DA TI
            </h4>
            <img src="/wp-content/uploads/2014/02/e-book.png" alt="">
        </div>
        <div class="fd_col_last">
            <h3>NO PLANEJAMENTO ESTRATÉGICO DAS EMPRESAS</h3>
            <p>Esse ebook ajudará a entender o real impacto da TI no planejamento estratégico da sua empresa!</p>
            <form action="javascript:void(0)" class="fd_abandon_form" id="fd_abandon_window">
                <div class="fd_group">
                    <input type="text" class="fd_input" name="aw_name" placeholder="Nome">
                </div>
                <div class="fd_group">
                    <input type="text" class="fd_input" name="aw_email" placeholder="E-mail">
                </div>
                <div class="fd_group">
                    <input type="text" class="fd_input" name="aw_cargo" placeholder="Cargo">
                </div>
                <button type="submit" class="button aw_button">BAIXAR EBOOK</button>
            </form>
        </div>
        <div id="msgFormAbandonWindow"></div>
    </div>
    <!-- END FD ABANDON WINDOW -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

    <script src="/wp-content/themes/Divi/js/jquery.validation.js"></script>
    <script src="/wp-content/themes/Divi/js/jquery.mask.min.js"></script>
	<?php wp_footer(); ?>


</body>
</html>