<?php

$errors = array();  	// array to hold validation errors
$data = array(); 		// array to pass back data

$data = $_POST['fd_name'];

require 'phpmailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

$nome     	= "";
$email    	= "";
$telefone  	= "";
$mensagem 	= "";

$nome		= $_POST['fd_name'];
$email    	= $_POST['fd_email'];
$telefone 	= $_POST['fd_phone'];
$empresa    = $_POST['fd_company'];
$mensagem 	= $_POST['fd_message'];
$servico    = $_POST['fd_service'];

if(empty($nome) || empty($email) || empty($telefone) || empty($mensagem) || empty($empresa)){
    header('', ':', 500);
    $data = "Sr(a) " . $nome . ",<br />Seu contato não pode ser enviado, por favor tente novamente";
    die(json_encode(array('error' => true, 'msg' => $data, 'data' => $_POST)));
}

$title    	= 'NetFive - Contato pelo Site';

// portas 587 - 25 - 2525 - 465
//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.mandrillapp.com:587';             // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'mandrill@fatordigital.com.br';     // SMTP username
$mail->Password = 'lWsaEFU2NG3pHx9ceOuvoQ';           // SMTP password de testes
//$mail->Password = ''  //PASSWORD DO CLIENTE
$mail->SMTPSecure = 'tls';                            // TCP port to connect to


$mail->AddAddress('contato@netfive.com.br');
// $mail->AddAddress('andre.carello@fatordigital.com.br');
// $mail->AddBCC('andre.carello@fatordigital.com.br'); // Cópia Oculta

$mail->From = "nao.responda@netfive.com.br"; // Seu e-mail
$mail->FromName = $title; // Seu nome
$mail->AddReplyTo($email,$nome);
// $mail->From = $email;
// $mail->FromName = $title;
//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
$mail->isHTML(true);                                    // Set email format to HTML

$mail->Subject = $title;
$mail->CharSet = 'UTF-8';
$msg = "<table width='100%' height='100%' border='0'><tr><td style='font-family: Arial, Helvetica, sans-serif;font-size: 13px;line-height: 150%;'>";
$msg .= "<h3>" . $title . "</h3>";
$msg .= "<h4>Gostaria de mais informações sobre: " . $servico . "</h4>";
$msg .= "<b>Nome: </b>" .$nome."<br>";
$msg .= "<b>Telefone: </b>" .$telefone."<br>";
$msg .= "<b>E-Mail: </b>" .$email."<br>";
$msg .= "<b>Empresa: </b>" .$empresa."<br>";
$msg .= "<b>Mensagem: </b>" . $mensagem ."<br>";
$msg .= "</td></tr></table>";
$mail->Body = $msg;
//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

header('Content-Type: Application/json');

if(!$mail->send()) {
    header('', ':', 500);
    $data = "Sr(a) " . $nome . ",<br />Seu contato não pode ser enviado, por favor tente novamente";
    die(json_encode(array('error' => true, 'msg' => $data, 'data' => $_POST)));
} else {
    header('', ':', 200);
    $data = "Sr(a) " . $nome . ",<br />Seu contato foi enviado com sucesso. <br />Entraremos em contato o mais breve possível";
    die(json_encode(array('error' => false, 'msg' => $data, 'data' => $_POST)));
}
